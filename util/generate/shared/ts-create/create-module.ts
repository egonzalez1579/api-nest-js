import * as fs from 'fs';
import * as Handlebars from 'handlebars';
import * as chalk from 'chalk';
import { SharedFunctions } from '../shared-functions/shared-functions';

export class Module {
  public dir = 'src/';
  public shFn = new SharedFunctions();

  constructor(public nameSchema: string, public namesTables: any) {
    this.createModuloPrincipal(nameSchema, namesTables);
  }

  createModuloPrincipal(nameSchema: string, namesTables: any) {
    const source = this.shFn.getTemplate('module');
    const template = Handlebars.compile(source);

    const nameModule = this.shFn.namePrimaryMayus(nameSchema);
    const data = { nameModule, modulos: namesTables };
    const content = template(data);

    const folder = `${this.dir}${nameSchema}/${this.shFn.replace(
      nameSchema,
    )}.module.ts`;

    if (this.shFn.verifyFolderExists(folder)) {
      return;
    }
    fs.writeFile(folder, content, err => {
      if (err) {
        console.log(err);
      } else {
        console.log(
          chalk.bold.yellow(`--- Modulo Creado: ${folder}`) +
            chalk.bold.green('✔'),
        );
      }
    });
  }
}
