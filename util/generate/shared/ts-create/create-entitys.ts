import { Formatos } from '../formatos/formatos';
import { SharedFunctions } from '../shared-functions/shared-functions';
import * as fs from 'fs';
import * as Handlebars from 'handlebars';
import * as chalk from 'chalk';

export class Entity {
  public dir = 'src/';
  public shFn = new SharedFunctions();
  public path: string;

  constructor(public nameSchema: string, public table: any) {
    this.path = `${this.dir}${this.shFn.replace(nameSchema)}`;
    this.onCreateEntity(nameSchema, table);
  }

  onCreateEntity(schemaName: string, table: any) {
    const columns = this.getColsForEntity(table);
    const tableName = table.name;

    const className = this.shFn.namePrimaryMayus(
      this.shFn.singularword(table.name),
    );
    const context = {
      schemaName,
      tableName,
      columns,
      className,
      ...this.getRelations(table),
    };

    this.createFile(context, table.name);
  }

  createFile(context: any, tableName: string) {
    const template = this.shFn.getTemplate('entity');

    const builder = Handlebars.compile(template);
    const entity = builder(context);
    const singularTableName = this.shFn.singularword(tableName);

    const path = `${this.path}/${this.shFn.replace(
      tableName,
    )}/${this.shFn.replace(singularTableName)}.entity.ts`;

    if (this.shFn.verifyFolderExists(path)) {
      return;
    }

    fs.writeFile(path, entity, err => {
      if (err) {
        console.log(err);
      } else {
        console.log(
          chalk.bold.yellow(`--- Entity Creado: ${path}`) +
            chalk.bold.green('✔'),
        );
      }
    });
  }

  getColsForEntity(table: any) {
    const columns = [];
    for (const col of table.columns) {
      const name = col.name;
      const typeInDB = col.type.name;

      const type = Formatos.getFormatos[typeInDB];

      if (name !== 'id') {
        const specialType = type === 'JSON';
        columns.push({ name, type, specialType });
      }
    }

    return columns;
  }

  getRelations(table: any) {
    const hasMany = [];
    const belongsTo = [];
    const belongsToMany = [];

    /**
     * @hasMany
     * 1 to m
     */
    for (const relation of table.o2mRelations) {
      const tableName = relation.targetTable.name;
      const name = this.shFn.namePrimaryMayus(
        this.shFn.singularword(tableName),
      );

      for (const fk of relation.foreignKey.columns) {
        const foreignKey = fk.name;
        hasMany.push({ name, foreignKey, tableName });
      }
    }

    // console.log(table.o2mRelations);

    /**
     * @belongsTo
     * m to 1
     */
    for (const relation of table.m2oRelations) {
      const tableName = relation.targetTable.name;
      const name = this.shFn.namePrimaryMayus(
        this.shFn.singularword(tableName),
      );

      for (const fk of relation.foreignKey.columns) {
        const foreignKey = fk.name;
        belongsTo.push({ name, foreignKey, tableName });
      }
    }

    const relations = { hasMany, belongsTo, belongsToMany };
    const imports = this.getImportsForEntity(relations);
    return { ...relations, imports };
  }

  getImportsForEntity(relations: any) {
    const imports = [];
    for (const relationType in relations) {
      if (relations.hasOwnProperty(relationType)) {
        const _relations = relations[relationType];
        for (const relation of _relations) {
          /**
           * @todo "verify if import exists"
           * @todo "verify if make reference to this"
           */        
          const singular = this.shFn.singularword(relation.tableName);
          const entityName = this.shFn.replace(singular);
          const folder = this.shFn.replace(relation.tableName);
          const name = this.shFn.primaryMayus(singular);
          const path = `src/${relation.tableName}/${folder}/${entityName}.entity`;
          imports.push({ name, path });
        }
      }
    }

    return imports;
  }
}
