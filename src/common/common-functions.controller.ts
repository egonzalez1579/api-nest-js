import {
  Controller,
  Delete,
  Put,
  Post,
  Get,
  Query,
  Param,
  Body,
  HttpException
} from '@nestjs/common'
import { CommonFunctionsService } from './common-functions.service'
import { Util } from './common-utils'

@Controller()
export class CommonFunctionsController {
  constructor(
    private readonly commonService: CommonFunctionsService,
    private readonly util: Util,
  ) {}

  @Get()
  async findAll(@Query() queryParams) {
    const parameters = this.util.getParameters(queryParams)
    try {
      return await this.commonService.findAll(parameters)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Get('count')
  async findAllCount(@Query() queryParams) {
    const parameters = this.util.getParameters(queryParams)
    try {
      return await this.commonService.finAllCountAll(parameters)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Get('one')
  async findOne(@Query() queryParams) {
    const parameters = this.util.getParameters(queryParams)

    try {
      return await this.commonService.findOne(parameters)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Get('/:id')
  async findByPk(@Param('id') id, @Query() queryParams) {
    const parameters = this.util.getParameters(queryParams)

    try {
      return await this.commonService.findByPk(id, parameters)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Post()
  async create(@Body() data, @Query() queryParams) {
    console.log(data)

    const parameters = this.util.getParameters(queryParams)

    try {
      return await this.commonService.create(data, parameters)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Post('multiple')
  async createMultiple(@Body() data, @Query() queryParams) {
    try {
      return await this.commonService.createMultipe(data, queryParams)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Put()
  async update(@Body() data, @Query() queryParams) {
    try {
      return await this.commonService.update(data, queryParams)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  @Delete('/:id')
  async delete(@Param('id') id, @Query() queryParams) {
    try {
      return await this.commonService.delete(id)
    } catch (error) {
      this.errorHandling(error)
    }
  }

  private errorHandling(err: any) {
    throw new HttpException(
      {
        name: err.name,
        message: err.message,
        detail: {
          errors: err.errors,
          fields: err.fields,
          original: err.original,
        },
      },
      500,
    )
  }
}
