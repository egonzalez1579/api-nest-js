import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as compression from 'compression';
import * as volleyball from 'volleyball';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(async (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    res.header('Authorization');
    next();
  });

  app.use(volleyball);
  app.use(compression());

  await app.listen(3000);
}

bootstrap();
