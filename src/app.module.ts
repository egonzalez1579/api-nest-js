import { Module } from '@nestjs/common';
import { routes } from './app.routes';
import { RouterModule } from 'nest-router';

@Module({
  imports: [RouterModule.forRoutes(routes)],
})
export class AppModule {}
